import { DashboardController } from './controllers/dashboardController';
import { NotificationController } from './controllers/notificationController';
import { TemplateController } from './controllers/templateController';
import { DataBootstrapService } from './services/dataBootsrapService';
import { SystemController } from './controllers/systemController';
import { UserController } from './controllers/userController';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as mongoose from 'mongoose';
import * as requestLogger from './middleware/requestLogger';
import * as authHandler from './middleware/authentication';
import * as cors from './middleware/cors';
import { Configuration } from './configuration';
import {GameController} from './controllers/gameController';
import {AuthenticationController} from './controllers/authenticationController';
import * as socketio from 'socket.io-client';
import * as jwt from 'jsonwebtoken';

export class WebApi {
    gameController: GameController;
    authenticationController: AuthenticationController;
    userController: UserController;
    systemController: SystemController;
    templateController: TemplateController;
    dataBootstrapService: DataBootstrapService;
    notificationController: NotificationController;
    dashboardController: DashboardController;

    /**
     * @param app - express application
     * @param port - port to listen on
     */
    constructor(private app: express.Express, private port: number) {
      mongoose.connect(Configuration.challenger_node_connection_string);
      let token = jwt.sign({serverId: 1}, Configuration.secret, {
              expiresIn: '24h'
            });
      let io: any = socketio(`${Configuration.notificationUri}?token=${token}`);
      let db = mongoose.connection;

      db.on('error', console.error.bind(console, 'connection error:'));
      db.once('open', function() {
        console.log('Connected to db!');
      });

      this.dataBootstrapService = new DataBootstrapService();
      this.authenticationController = new AuthenticationController(mongoose);
      this.gameController = new GameController(mongoose, io);
      this.userController = new UserController(mongoose);
      this.systemController = new SystemController(mongoose);
      this.templateController = new TemplateController(mongoose);
      this.notificationController = new NotificationController(mongoose);
      this.dashboardController = new DashboardController(mongoose);
      this.configureData();
      this.configureMiddleware(app);
      this.configureRoutes(app);
    }

    private configureData() {
      this.dataBootstrapService.configure();
    }

    /**
     * @param app - express application
     */
    private configureMiddleware(app: express.Express) {
        app.use(requestLogger);
        app.use(cors);
        app.use('/api', authHandler);
    }

    private configureRoutes(app: express.Express) {
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(bodyParser.json());
        app.use('/api/game', this.gameController.controller);
        app.use('/api/user', this.userController.controller);
        app.use('/api/system', this.systemController.controller);
        app.use('/api/template', this.templateController.controller);
        app.use('/api/notification', this.notificationController.controller);
        app.use('/api/dashboard', this.dashboardController.controller);
        app.use('/authentication', this.authenticationController.controller);
        // mount more routers here
        // e.g. app.use("/organisation", organisationRouter);
    }

    public run() {
        this.app.listen(this.port);
    }
}