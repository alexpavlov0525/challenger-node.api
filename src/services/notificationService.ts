import { MessageType } from './../enums/message-type-enum';
import { Configuration } from './../configuration';
import 'isomorphic-fetch';

export class NotificationService {
  io: any;
  constructor(io: any) {
    this.io = io;
  }

  sendStartGame(sender: string, templateName: string, recipients: string[], gameId: any, notificationId: string) {
    let payload = {
      notificationId: notificationId,
      recipients: recipients,
      message: `${sender} has invited you to join ${templateName}`,
      gameId: gameId,
      messageType: MessageType.JoinGame
    }
    this.io.emit('startgame', payload);
  }

  sendJoinedGame(sender: string, templateName: string, recipients: string[]) {
    let payload = {
      recipients: recipients,
      message: `${sender} has joined ${templateName}`
    }
    this.io.emit('joinedgame', payload);
  }

  sendStartRound(sender: string, templateName: string, round: any, recipients: string[], gameId: any, notificationId: string) {
    let payload = {
      notificationId: notificationId,
      recipients: recipients,
      message: `${sender} wants to start round # ${Number(round)+1} of ${templateName}`,
      gameId: gameId,
      round: round,
      messageType: MessageType.StartRound
    }
    this.io.emit('startround', payload);
  }

  sendStartedRound(recipients: string[]) {
    let payload = {
      recipients: recipients,
      message: 'started round'
    }
    this.io.emit('startedround', payload);
  }

  sendFinishRound(message: string, recipients: string[], gameId: any, round: number, notificationId: string) {
    let payload = {
      notificationId: notificationId,
      recipients: recipients,
      message: message,
      gameId: gameId,
      round: round,
      messageType: MessageType.FinishRound
    }
    this.io.emit('finishround', payload);
  }

  sendFinishedRound(recipients: string[]) {
    let payload = {
      recipients: recipients,
      message: 'finished round'
    }
    this.io.emit('finishedround', payload);
  }

  sendFinishedGame(recipients: string[]) {
    let payload = {
      recipients: recipients,
      message: 'finished game'
    }
    this.io.emit('finishedgame', payload);
  }
}