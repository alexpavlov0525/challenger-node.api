import { System } from './../models/system';
import * as winston from 'winston';

export class DataBootstrapService {
  constructor() {
  }

  configure() {
    this.setupSystemCollectionIfNone();
  }

  private setupSystemCollectionIfNone() {
    System.findOne().then(system => {
      if (!system) {
        let newSystem = new System({
        });

        newSystem.canCreate = true;

        newSystem.save().then((newSystem) => {
          winston.info(`DATA BOOTSTRAP|${(new Date()).toUTCString()}|successfully created system record`);
        }).catch(error => {
          winston.error(`DATA BOOTSTRAP|${(new Date()).toUTCString()}|failed to create system record. error: ${error}`);
        });
      }
    });
  }
}