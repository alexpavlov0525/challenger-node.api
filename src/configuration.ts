export class Configuration {
    static api_host_port: number = 5001;
    static db_host_port: number = 27017;
    static iterations: number = 10000;
    static keylen: number = 512;
    static digest: string = 'sha512';
    // db_host: string = process.env.node_env === 'local' ? 'localhost' : 'mongo.challengernode.com:80';
    static db_user: string = 'challenger-node-db';
    static db_pwd: string = 'r7X06EzywXT6j$!^K9AAJ2eH';
    static challenger_node_connection_string: string = `mongodb://${Configuration.db_user}:${Configuration.db_pwd}@localhost/challenger-node`;
    static notificationUri: string = process.env.node_env === 'production' ? 'http://localhost:6001' : 'http://localhost:6001';
    static secret: string = 'asdlikhwqkmdvnoqsfhqwfdaspfjasf';
}