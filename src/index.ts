import { Configuration } from './configuration';
import express = require('express');
import { WebApi } from './application';

let port = Configuration.api_host_port;

let api = new WebApi(express(), port);
api.run();
console.info(`listening on ${port}`);