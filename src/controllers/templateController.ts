import { Template, TemplateSchema } from './../models/template';
import * as express from 'express';
import { Controller } from './controller';
import * as jwt from 'jsonwebtoken';

export class TemplateController extends Controller {
  controller = express.Router();

  constructor(mongoose: any) {
    super(mongoose);
    this.configure();
  }

  configure() {
    this.controller.get('/', async (request: express.Request, response: express.Response) => {
      Template.find().then(templates => {
        response.send({templates});
      });
    });

    this.controller.get('/:id', async (request: express.Request, response: express.Response) => {
      Template.findById(request.params.id).then(template => {
        response.send(template);
      }).catch(error => {
        response.statusCode = 404;
        response.send({ success: false, message: 'Not Found!' });
      });
    });

    this.controller.put('/:id', async (request: express.Request, response: express.Response) => {
      let updatedTemplate = {
        name: request.body.name,
        description: request.body.description,
        createdBy: request.body.createdBy,
        minPlayers: request.body.minPlayers,
        maxPlayers: request.body.maxPlayers,
        rounds: request.body.rounds,
        roundsToWin: request.body.roundsToWin,
        lastEdited: Date()
      };

      let errors = await this.validate(request.body);

      let token = jwt.decode(request.headers['authorization']);

      let existingTemplate = await Template.findOne({_id: request.body._id});

      if (token.username !== existingTemplate.createdBy) {
        errors.push('You can only edit templates you have created');
      }

      if (errors && errors.length > 0) {
        response.statusCode = 400;
        return response.send({ success: false, message: 'Validation failed', errors: errors});
      }

      Template.findByIdAndUpdate(request.params.id, updatedTemplate).then(template => {
        response.send({ success: true, message: 'Successfully updated!'});
      }).catch(error => {
        response.statusCode = 400;
        response.send({ success: false, message: error.message });
      });
    });

    this.controller.post('/', async (request, response, next) => {
      let template = new Template ({
        name: request.body.name,
        description: request.body.description,
        createdBy: request.body.createdBy,
        minPlayers: request.body.minPlayers,
        maxPlayers: request.body.maxPlayers,
        rounds: request.body.rounds,
        roundsToWin: request.body.roundsToWin,
        dateCreated: Date()
      });

      let errors = await this.validate(request.body);

      if (errors && errors.length > 0) {
        response.statusCode = 400;
        return response.send({ success: false, message: 'Validation failed', errors: errors});
      }

      template.save().then((template) => {
        response.send({ success: true, message: 'Successfully added!' });
      }).catch(error => {
        response.statusCode = 400;
        response.send({ success: false, message: error.message });
      });
    });
  }

  async validate(entity: any) {
    let errors = [];

    let exisitingTemplates =  await Template.find({name: entity.name});
    let isUnique = true;

    exisitingTemplates.forEach(t => {
      if (String(entity._id) !== String(t._id)) {
        isUnique = false;
      }
    });

    if (!isUnique) {
      errors.push('Template with this name already exists');
      return errors;
    }

    if (!entity.name) {
      errors.push('Name is required');
    }

    if (!entity.minPlayers) {
      errors.push('Min Players is required');
    }

    if (!entity.maxPlayers) {
      errors.push('Max Players is required');
    }

    if (entity.minPlayers > entity.maxPlayers) {
      errors.push('Min Players has to be less than Max Players');
    }

    if (!entity.rounds) {
      errors.push('Rounds is required');
    }

    if (!entity.roundsToWin) {
      errors.push('Rounds to win is required');
    }

    if (entity.roundsToWin > entity.rounds) {
      errors.push('Rounds to win has to be less than Rounds');
    }

    return errors;
  }
}