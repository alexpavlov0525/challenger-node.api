import { System } from './../models/system';
import { Controller } from './controller';
import * as express from 'express';

export class SystemController extends Controller {
  controller = express.Router();

  constructor(mongoose: any) {
    super(mongoose);
    this.configure();
  }

  configure() {
    this.controller.get('/cancreate', async (request, response, next) => {
      await System.findOne().then(system => {
        if (system) {
          response.send(system.canCreate);
        } else {
          response.send(false);
        }
      });
    });

    this.controller.put('/', async (request, response, next) => {
      let system = await System.findOne();
      system.canCreate = request.body.canCreate;
      await System.update({_id: system._id}, system);
      response.statusCode = 200;
      response.send({success: true, message: 'Successfully updated!'});
    });
  }
}