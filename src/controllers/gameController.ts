import { GameStatus } from './../enums/game-status-enum';
import { MessageType } from './../enums/message-type-enum';
import { Notification } from './../models/notification';
import { NotificationService } from './../services/notificationService';
import { Template } from './../models/template';
import { User } from './../models/user';
import { Game, GameSchema } from './../models/game';
import * as express from 'express';
import { Controller } from './controller';
import * as jwt from 'jsonwebtoken';
import { RoundStatus } from './../enums/round-status-enum';


export class GameController extends Controller {
  controller = express.Router();
  notificationService: NotificationService;
  io: any;
  constructor(mongoose: any, io: any) {
    super(mongoose);
    this.io = io;
    this.notificationService = new NotificationService(this.io);
    this.configure();
  }

  async createNotification(gameId: string, to: string, message: string, messageType: string, roundId: number = null) {
    let notification = new Notification({
      dateCreated: new Date(),
      roundId: roundId,
      gameId: gameId,
      to: to,
      message: message,
      messageType: messageType
    });

    return await notification.save();
  }

  configure() {
    this.controller.get('/', async (request: express.Request, response: express.Response) => {
      let count = request.body.count ? request.body.count : 10;
      let token = jwt.decode(request.headers['authorization']);
      let games = await Game.find({players : token.username}).sort({dateStarted: -1});

      let finishedGames = games.filter(x => x.status === GameStatus.Finished);
      let inProgressGames = games.filter(x => x.status === GameStatus.InProgress);
      let awaitingPlayersGames = games.filter(x => x.status === GameStatus.AwaitingPlayers);
      let indecisiveGames = games.filter(x => x.status === GameStatus.Indecisive);

      games = [];

      games = games.concat(awaitingPlayersGames);
      games = games.concat(inProgressGames);
      games = games.concat(indecisiveGames);
      games = games.concat(finishedGames);

      response.statusCode = 200;
      response.send({games: games});
    });

    /**
     * Send notifications to the rest of the players
     * about starting a round # roundId
     */
    this.controller.put('/:id/round/:roundId/start', async (request: express.Request, response: express.Response) => {
      // get the game
      let game = await Game.findById(request.params.id);
      // get the template
      let template = await Template.findById(game.templateId);

      let token = jwt.decode(request.headers['authorization']);
      let username = token.username;

      // if no game return 404
      if (!game) {
        response.statusCode = 404;
        return response.send({ success: false, message: 'Not Found!' });
      }

      // if game is over return 301
      if (game.status === GameStatus.Finished || game.status === GameStatus.Indecisive) {
        response.statusCode = 301;
        return response.send({ success: false, message: 'The game is over already!' });
      }

      // if game is awaiting players
      if (game.status === GameStatus.AwaitingPlayers) {
        response.statusCode = 301;
        return response.send({ success: false, message: 'Not all players have joined the game!' });
      }

      // if round has finished
      if (game.rounds[request.params.roundId].status !== RoundStatus.ReadyToStart) {
        response.statusCode = 301;
        return response.send({ success: false, message: 'Round is not in proper state!' });
      }

      // create notifications for all users but self
      game.players.filter(x => x !== username).forEach(async (player) => {
        let notification = await this.createNotification(
          request.params.id,
          player,
          `${username} wants to start round # ${Number(request.params.roundId)+1} of ${template.name}`,
          MessageType.StartRound,
          request.params.roundId
        );

        // send notifications to all players. but the requestor
        this.notificationService
          .sendStartRound(
            token.username,
            template.name,
            request.params.roundId,
            [player],
            game._id,
            notification._id
            );
      });

      // add self to the list of ready users for the round # roundId
      game.rounds[request.params.roundId].readyPlayers.push(username);

      // set round status to awaiting players
      game.rounds[request.params.roundId].status = RoundStatus.AwaitingPlayers;

      // update game and verify
      await Game.findByIdAndUpdate(request.params.id, game);
      let updatedGame = await Game.findById(request.params.id);
      if (!updatedGame) {
        response.statusCode = 400;
        response.send({ success: false, message:  'Didn\'t update!'});
      }

      response.send({ success: true, message: 'Successfully updated!'});
    });

    /**
     * Agree to start a round
     */
    this.controller.put('/:id/round/:roundId/start/agree', async (request: express.Request, response: express.Response) => {
      // get the game
      let game = await Game.findById(request.params.id);

      let token = jwt.decode(request.headers['authorization']);
      let username = token.username;

      // if no game return 404
      if (!game) {
        response.statusCode = 404;
        return response.send({ success: false, message: 'Not Found!' });
      }

      // if game is over return 301
      if (game.status === GameStatus.Finished) {
        response.statusCode = 301;
        return response.send({ success: false, message: 'The game is over already!' });
      }

      // add self to the list of ready users for the round # roundId
      game.rounds[request.params.roundId].readyPlayers.push(username);
      let everyoneIsReady = true;

      game.players.forEach((player: any) => {
        if (game.rounds[request.params.roundId].readyPlayers.indexOf(player) < 0) {
          everyoneIsReady = false;
        }
      });

      if (everyoneIsReady) {
        game.rounds[request.params.roundId].startDate = new Date();
        game.rounds[request.params.roundId].status = RoundStatus.InProgress;
      }

      // remove notification that resulted in this call
      let notification = await Notification.findById(request.body.notificationId);
      if (notification) {
        await notification.remove();
      }

      // update game and verify
      await Game.findByIdAndUpdate(request.params.id, game);
      let updatedGame = await Game.findById(request.params.id);
      if (!updatedGame) {
        response.statusCode = 400;
        response.send({ success: false, message:  'Didn\'t update!'});
      }

      this.notificationService.sendStartedRound(game.players);

      response.statusCode = 200;
      response.send({ success: true, message: 'Successfully updated!'});

    });

    /**
     * Propose finishing a round
     */
    this.controller.put('/:id/round/:roundId/finish', async (request: express.Request, response: express.Response) => {
      // get the game
      let game = await Game.findById(request.params.id);

      // get the template
      let template = await Template.findById(game.templateId);

      let token = jwt.decode(request.headers['authorization']);
      let username = token.username;

      // if no game return 404
      if (!game) {
        response.statusCode = 404;
        return response.send({ success: false, message: 'Not Found!' });
      }

      // if game is over return 301
      if (game.status === GameStatus.Finished) {
        response.statusCode = 301;
        return response.send({ success: false, message: 'The game is over already!' });
      }

      // if round has finished
      if (game.rounds[request.params.roundId].status !== RoundStatus.InProgress) {
        response.statusCode = 301;
        return response.send({ success: false, message: 'Round is not in proper state!' });
      }

      let message = `${username} claims that ${request.body.winner} won round # ${Number(request.params.roundId)+1} of ${template.name}`;

      // create notifications for all users but self
      game.players.filter(x => x !== username).forEach(async (player) => {
        let notification = await this.createNotification(
          request.params.id,
          player,
          message,
          MessageType.FinishRound,
          request.params.roundId
        );

        // send notifications to all players. but the requestor
        this.notificationService
          .sendFinishRound(
            message,
            [player],
            game._id,
            request.params.roundId,
            notification._id
          );
      });

      // clear ready players array
      game.rounds[request.params.roundId].readyPlayers = [];

      // add self to the list of ready users for the round # roundId
      game.rounds[request.params.roundId].readyPlayers.push(username);

      // set round status to awaiting players
      game.rounds[request.params.roundId].status = RoundStatus.AwaitingConfirmation;
      game.rounds[request.params.roundId].winner = request.body.winner;

      // update game and verify
      await Game.findByIdAndUpdate(request.params.id, game);
      let updatedGame = await Game.findById(request.params.id);
      if (!updatedGame) {
        response.statusCode = 400;
        response.send({ success: false, message:  'Didn\'t update!'});
      }

      response.send({ success: true, message: 'Successfully updated!'});

    });

    /**
     * Agree to finish a round
     */
    this.controller.put('/:id/round/:roundId/finish/agree', async (request: express.Request, response: express.Response) => {
      // get the game
      let game = await Game.findById(request.params.id);

      // get the template
      let template = await Template.findById(game.templateId);

      let token = jwt.decode(request.headers['authorization']);
      let username = token.username;

      // if no game return 404
      if (!game) {
        response.statusCode = 404;
        return response.send({ success: false, message: 'Not Found!' });
      }

      // if game is over return 301
      if (game.status === GameStatus.Finished) {
        response.statusCode = 301;
        return response.send({ success: false, message: 'The game is over already!' });
      }

      // add self to the list of ready users for the round # roundId
      game.rounds[request.params.roundId].readyPlayers.push(username);
      let everyoneIsReady = true;

      game.players.forEach((player: any) => {
        if (game.rounds[request.params.roundId].readyPlayers.indexOf(player) < 0) {
          everyoneIsReady = false;
        }
      });

      if (everyoneIsReady) {
        game.rounds[request.params.roundId].finishDate = new Date();
        game.rounds[request.params.roundId].status = RoundStatus.Finished;

        let results: {[name: string]: number} = {};

        game.rounds.forEach(round => {
          if(round.winner) {
            if (results[round.winner]) {
              results[round.winner] +=1;
            } else {
              results[round.winner] = 1;
            }
          }
        });

        game.players.forEach((player) => {
          if (results[player] === template.roundsToWin) {
            game.winner = player;
            game.status = GameStatus.Finished
            game.dateFinished = new Date();
          }
        });

        if (!game.winner) {
          game.rounds[Number(request.params.roundId)+1].status = RoundStatus.ReadyToStart;
        }
      }

      // remove notification that resulted in this call
      let notification = await Notification.findById(request.body.notificationId);
      if (notification) {
        await notification.remove();
      }

      // update game and verify
      await Game.findByIdAndUpdate(request.params.id, game);

      let updatedGame = await Game.findById(request.params.id);
      if (!updatedGame) {
        response.statusCode = 400;
        response.send({ success: false, message:  'Didn\'t update!'});
      }

      this.notificationService.sendFinishedRound(game.players);

      if (game.winner) {
        this.notificationService.sendFinishedGame(game.players);
      }

      response.statusCode = 200;
      response.send({ success: true, message: 'Successfully updated!'});

    });

    /**
     * Get game with :id
     */
    this.controller.get('/:id', async (request: express.Request, response: express.Response) => {
      let game = await Game.findById(request.params.id);
      if (!game) {
        response.statusCode = 404;
        response.send({ success: false, message: 'Not Found!' });
      }
      response.statusCode = 200;
      response.send({ success: true, message: 'Got your game!', payload: game });
    });

    /**
     * Join the game, remove notification,
     * send notification to players about the change,
     * once all players have joined - change status to InProgress
     */
    this.controller.put('/:id/join', async (request, response, next) => {
      // get the game based on params id
      let game = await Game.findById(request.params.id);
      let template = await Template.findById(game.templateId);

      // if no such game reutrn 400
      if (!game) {
        response.statusCode = 404;
        return response.send({ success: false, message: 'Not Found!' });
      }

      // if game is finished return 400
      if (game.status === GameStatus.Finished) {
        response.statusCode = 301;
        return response.send({ success: false, message: 'The game is over already!' });
      }

      // remove notification that resulted in this call
      let notification = await Notification.findById(request.body.notificationId);
      if (notification) {
        await notification.remove();
      }

      let token = jwt.decode(request.headers['authorization']);

      // add self to array of ready users
      game.readyPlayers.push(token.username);

      // check if everyone is ready
      let everyoneIsReady = true;

      game.players.forEach(player => {
        if (game.readyPlayers.indexOf(player) < 0) {
          everyoneIsReady = false;
        }
      });

      // if everyone is ready set the start game, change status to InProgress
      if (everyoneIsReady) {
        game.status = GameStatus.InProgress;
        game.dateStarted = new Date();
        game.rounds[0].status = RoundStatus.ReadyToStart;
      }

      // update game and verify
      await Game.findByIdAndUpdate(request.params.id, game);
      let updatedGame = await Game.findById(request.params.id);
      if (!updatedGame) {
        response.statusCode = 400;
        response.send({ success: false, message:  'Didn\'t update!'});
      }

      this.notificationService
        .sendJoinedGame(
          token.username,
          template.name,
          game.players);

      response.statusCode = 200;
      response.send({ success: true, message: 'Successfully updated!' });
    });

    /**
     * Creates initial game, creates notifications for each
     * player to join, sends notifications to players.
     * Set status to AwaitingPlayers
     */
    this.controller.post('/', async (request, response, next) => {

      // create initial game object
      let game = new Game({
        dateCreated: new Date(),
        name: null,
        templateId: request.body.templateId,
        winner: null,
        players: request.body.players,
        rounds: [],
        status: GameStatus.AwaitingPlayers
      });

      // if no template id return 400
      if (!game.templateId) {
        response.statusCode = 400;
        return response.send({success: false, message: 'No template provided!'})
      }

      let template = await Template.findById(game.templateId);

      // if no template matches template id return 400
      if (!template) {
        response.statusCode = 400;
        return response.send({success: false, message: 'Can\'t find template!'})
      }

      game.name = template.name;

      // make sure all players exist
      let allPlayersExist = true;

      if (!game.players) {
        allPlayersExist = false;
      } else {
        game.players.forEach(async (player) => {
          if (game.players.filter(x => x === player).length > 1) {
            response.statusCode = 400;
            return response.send({success: false, message: 'Can not have same player listed twice or more!'})
          }

          let user = await User.findOne({username: player});
          if (!user) {
            allPlayersExist = false;
          }
        });
      }

      // if not all players exist return 400
      if (!allPlayersExist) {
        response.statusCode = 400;
        return response.send({success: false, message: 'Some players don\'t exist!'})
      }

      // init round objects
      for (let i = 0; i < template.rounds; i ++) {
        game.rounds.push({
          startDate: null,
          finishDate: null,
          winner: null,
          status: RoundStatus.NotStarted,
          readyPlayers: []
        });
      }

      let token = jwt.decode(request.headers['authorization']);

      game.readyPlayers = [token.username];

      // insert the game
      let savedGame = await game.save();

      let existingGame = await Game.findById(savedGame._id);

      // verify insert success. if not return 400
      if (!existingGame) {
        response.statusCode = 400;
        return response.send({success: false, message: 'Game was not created'});
      }

      // create notification object for each notification
      game.players.filter(x => x !== token.username).forEach(async (player) => {
        let notification = await this.createNotification(
          savedGame._id,
          player,
          `${token.username} invited you to join a game of ${template.name}`,
          MessageType.JoinGame
        );
          // send notifications to all players. but ther requestor
        this.notificationService
          .sendStartGame(
            token.username,
            template.name,
            [player],
            game._id,
            notification._id);
      });

      response.send({ success: true, message: 'Successfully added!', payload: game });
    });

    this.controller.delete('/:id', async (request: express.Request, response: express.Response) => {
      Game.findByIdAndRemove(request.params.id).then(() => {
        response.send({ success: true, message: 'Successfully removed!' });
      }).catch(error => {
        response.statusCode = 400;
        response.send({ success: false, message: error.message });
      });
    });
  }
}