import { User, UserSchema } from './../models/user';
import * as express from 'express';
import * as jwt from 'jsonwebtoken';
import { Configuration } from './../configuration';
import { Controller } from './controller';
import * as crypto from 'crypto';


export class AuthenticationController extends Controller {
  controller = express.Router();

  constructor(mongoose: any) {
    super(mongoose);
    this.configure();
  }

  private async generateHashedPassword(passwordAttempt: string,
    salt: string,
    iterations: number,
    keylen: number,
    digest: string): Promise<string> {
    const hashAttempt = crypto.pbkdf2Sync(passwordAttempt, new Buffer(salt, 'hex'), iterations, keylen, digest);
    return hashAttempt.toString('hex');
  }

  private async verifyPassword(passwordAttempt: string,
    salt: string,
    iterations: number,
    keylen: number,
    digest: string,
    actualHash: string): Promise<boolean> {
    return await this.generateHashedPassword(passwordAttempt, salt, iterations, keylen, digest) === actualHash;
  }

  async findUser(username: string) {
    return User.findOne({ username: username }).exec();
  }

  configure() {
    this.controller.post('/create', async (request, response, next) => {
      let existingUser = await this.findUser(request.body.username);

      if (existingUser) {
        response.statusCode = 400;
        return response.send({ success: false, message: 'Username already exists.' });
      }

      if (!request.body.username) {
        response.statusCode = 400;
        return response.send({ success: false, message: 'Invalid username.' });
      };

      let user = new User();
      console.log(request.body);
      user.username = request.body.username;
      user.salt = crypto.randomBytes(512).toString('hex');
      user.hash = await this.generateHashedPassword(request.body.password,
        user.salt,
        Configuration.iterations,
        Configuration.keylen,
        Configuration.digest);
      user.dateCreated = new Date();

      await user.save();

      let createdUser = await User.findOne({ username: user.username });
      if (!createdUser) {
        response.statusCode = 400;
        return response.send({ success: false, message: 'Was not able to find new user' });
      }

      response.statusCode = 200;

      let tokenObject = {
        username: user.username
      };

      let token = jwt.sign(tokenObject, Configuration.secret, {
        expiresIn: '24h'
      });

      return response.send({
        success: true,
        message: 'Enjoy your token!',
        token: token
      });

      // response.send({ success: true, message: 'Successfully added new user' });
      // user.save().then(() => {
      //   let createdUser = User.findOne({username: user.username});
      //   if (!createdUser) {
      //     response.statusCode = 400;
      //     console.log('----------------2');
      //     return response.send({ success: false, message: 'Was not able to find new user'});
      //   }
      //   response.statusCode = 200;
      //   console.log('----------------3');
      //   response.send({ success: true, message: 'Successfully added new user'});
      // }).catch((error) => {
      //   response.statusCode = 400;
      //   console.log('----------------4');
      //   response.send({ success: false, message: 'Something went wrong..'});
      // });
    });

    this.controller.post('/authenticate', async (request, response, next) => {
      if (!request.body.username) {
        response.statusCode = 401;
        return response.send({ success: false, message: 'Authentication failed.' });
      }
      User.findOne({
        username: request.body.username
      }).then(async (user) => {
        if (!user) {
          response.statusCode = 401;
          return response.send({ success: false, message: 'Authentication failed. No such user.' });
        } else {
          let isPasswordCorrect = await this.verifyPassword(request.body.password,
            user.salt,
            Configuration.iterations,
            Configuration.keylen,
            Configuration.digest,
            user.hash);
          if (!isPasswordCorrect) {
            response.statusCode = 401;
            return response.send({ success: false, message: 'Authentication failed. Wrong password.' });
          } else {
            let tokenObject = {
              username: user.username,
              roles: user.roles
            };
            let token = jwt.sign(tokenObject, Configuration.secret, {
              expiresIn: '24h'
            });

            return response.send({
              success: true,
              message: 'Enjoy your token!',
              token: token
            });
          }
        }

      }).catch(error => {
        response.statusCode = 401;
        return response.send({ success: false, message: error.message });
      });
    });
  }
}