import { Notification } from './../models/notification';
import { Controller } from './controller';
import * as express from 'express';
import * as jwt from 'jsonwebtoken';

export class NotificationController extends Controller{
  controller = express.Router();
  constructor(mongoose: any) {
    super(mongoose);
    this.configure();
  }

  configure() {
    this.controller.get('/', async (request: express.Request, response: express.Response) => {
      let token = jwt.decode(request.headers['authorization']);
      let notifications = await Notification.find({to: token.username}).sort({dateCreated: -1});

      response.statusCode = 200;
      response.send({notifications: notifications});
    });

    this.controller.get('/counter', async (request: express.Request, response: express.Response) => {
      let token = jwt.decode(request.headers['authorization']);
      let counter = await Notification.find({to: token.username}).count();

      response.statusCode = 200;
      response.send({counter: counter});
    });
  }
}