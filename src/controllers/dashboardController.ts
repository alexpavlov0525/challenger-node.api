import { Stat } from './../models/stat';
import { Template } from './../models/template';
import { User } from './../models/user';
import { Game, GameSchema } from './../models/game';
import * as express from 'express';
import { Controller } from './controller';
import * as jwt from 'jsonwebtoken';

export class DashboardController extends Controller {
  controller = express.Router();

  constructor(mongoose: any) {
    super(mongoose);
    this.configure();
  }

  configure() {
    this.controller.get('/', async (request: express.Request, response: express.Response) => {
      let token = jwt.decode(request.headers['authorization']);
      let stat = await Stat.findOne({username: token.username});
      response.statusCode = 200;
      response.send({stat: stat});
    });
  }
}