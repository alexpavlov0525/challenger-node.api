import { User, UserSchema } from './../models/user';
import * as express from 'express';
import * as jwt from 'jsonwebtoken';
import { Controller } from './controller';


export class UserController extends Controller {
  controller = express.Router();

  constructor(mongoose: any) {
    super(mongoose);
    this.configure();
  }

  configure() {
    this.controller.get('/', async (request: express.Request, response: express.Response) => {
      let token = jwt.decode(request.headers['authorization']);
      let requestingUser = token.username;

      let users = await User.find();

      users = users.filter(x => x.username !== requestingUser);

      response.send({success: true, users: users.map(user =>{return user.username})});
    });

    this.controller.get('/:id', async (request: express.Request, response: express.Response) => {
      User.findById(request.params.id).then(user => {
        return response.send(user);
      }).catch(error => {
        response.statusCode = 404;
        return response.send({ success: false, message: 'Not Found!' });
      });
    });

    this.controller.get('/get/profile', async (request: express.Request, response: express.Response) => {
      let token = request.headers['authorization'];
      if (token) {
        let decoded = jwt.decode(token);
        if (decoded) {
          return response.send({success: true, message: 'You got it!', payload: decoded});
        } else {
          return response.send({ success: false, message: 'Wrong token!' });
        }
      } else {
        return response.send({ success: false, message: 'No match found!' });
      }
    });
    // this.controller.put('/:id', (request: express.Request, response: express.Response) => {
    //   let updatedUser = {

    //   };

    //   User.findByIdAndUpdate(request.params.id, updatedUser).then(user => {
    //     response.send({ success: true, message: 'Successfully updated!', payload: user });
    //   }).catch(error => {
    //     response.statusCode = 400;
    //     response.send({ success: false, message: error.message });
    //   });
    // });

    // this.controller.post('/', (request, response, next) => {
    //   let user = new User({
    //   });

    //   user.save().then((user) => {
    //     response.send({ success: true, message: 'Successfully added!', payload: user });
    //   }).catch(error => {
    //     response.statusCode = 400;
    //     response.send({ success: false, message: error.message });
    //   });
    // });
  }
}