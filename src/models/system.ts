import { Document, Schema, Model, model} from 'mongoose';
import { ISystem } from './isystem';

export interface ISystemModel extends ISystem, Document {

}

export let SystemSchema: Schema = new Schema({
  canCreate: Boolean
});

export const System: Model<ISystemModel> = model<ISystemModel>('System', SystemSchema);