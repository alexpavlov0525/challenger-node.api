import * as mongoose from 'mongoose';

export interface ISystem extends mongoose.Document {
  canCreate: Boolean;
}