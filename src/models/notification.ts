import { Document, Schema, Model, model} from 'mongoose';
import {INotification} from './inotification';

export interface INotificationModel extends INotification, Document {
}

export let NotificationSchema: Schema = new Schema({
  dateCreated: Date,
  gameId: String,
  roundId: Number,
  to: String,
  message: String,
  messageType: String
});

NotificationSchema.pre('save', next => {
  next();
});

export const Notification: Model<INotificationModel> = model<INotificationModel>('Notification', NotificationSchema);