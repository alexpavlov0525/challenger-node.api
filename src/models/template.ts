import { Document, Schema, Model, model } from 'mongoose';
import { ITemplate } from './itemplate';

export interface ITemplateModel extends ITemplate, Document {
}

export let TemplateSchema: Schema = new Schema({
    name: String,
    description: String,
    createdBy: String,
    minPlayers: Number,
    maxPlayers: Number,
    rounds: Number,
    roundsToWin: Number,
    dateCreated: Date,
    lastEdited: Date
});

TemplateSchema.pre('save', next => {
  next();
});

export const Template: Model<ITemplateModel> = model<ITemplateModel>('Template', TemplateSchema);