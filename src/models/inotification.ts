import * as mongoose from 'mongoose';

export interface INotification extends mongoose.Document {
  dateCreated: Date;
  gameId: string;
  roundId: number;
  to: string;
  message: string;
  messageType: string;
}