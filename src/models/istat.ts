import * as mongoose from 'mongoose';

export interface IStat extends mongoose.Document {
    dateCreated: Date;
    username: string;
    templates: any[];
}