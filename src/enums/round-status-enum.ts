export enum RoundStatus {
  NotStarted,
  AwaitingPlayers,
  InProgress,
  ReadyToStart,
  AwaitingConfirmation,
  Finished
}