import * as express from 'express';
import * as jwt from 'jsonwebtoken';
import { Configuration } from './../configuration';

let allowAnonymousFor = [
  '/system/cancreate'
];

let authHandler: express.RequestHandler = (
    request: express.Request,
    response: express.Response,
    next: express.NextFunction
) => {
    if (request.method === 'OPTIONS') {
      next();
    } else if (allowAnonymousFor.indexOf(request.url) > -1) {
      next();
    } else {
      let token = request.headers['authorization'];
      if (token) {
        jwt.verify(token, Configuration.secret, (error, decoded) => {
          if (error) {
            console.log('Failed to authenticate token.');
            response.statusCode = 401;
            return response.send({ success: false, message: 'Failed to authenticate token.' });
          } else {
            next();
          }
        });
      } else {
        response.statusCode = 401;
        console.log('No token provided.');
        response.send({
          success: false,
          message: 'No token provided.'
      });
      }
    }
};

export = authHandler;