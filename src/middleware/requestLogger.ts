import * as express from 'express';
import * as winston from 'winston';

winston.add(winston.transports.File, {filename: 'api.log'});

let requestLogger: express.RequestHandler = (
    request: express.Request,
    response: express.Response,
    next: express.NextFunction
) => {
    winston.info(`REQUEST |${(new Date()).toUTCString()}|${request.method}|${request.url}|${request.ip}`);
    next();
};

export = requestLogger;